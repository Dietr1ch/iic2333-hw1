/*
 * Poor man's `parallel`
*/
#include <getopt.h>    // argument parsing
#include <stdio.h>     // standard I/O
#include <string.h>    // strlen

#include <unistd.h>    // fork, exec, sysconf
#include <stdlib.h>    // exit
#include <sys/wait.h>  // wait

#define MAX(x, y) ( ((x)>(y)) ? (x) : (y) )
#define FREE(x) if(x){free(x); x=NULL;}

// Argument handling
const char *argp_program_version = "pmp 0.1a";
const char *argp_program_bug_address = "<rector@uc.cl>";
static char doc[] = "Poor man's `parallel` -- a crappy clone of GNU's parallel";




// Program Arguments
// =================
int MAX_JOBS = 1;

void
printHelp(char* programName) {
	printf("Poor man's GNU parallel\n");
	printf("	Runs jobs from stdin\n");

	printf("usage: %s [-j PARALLEL_JOBS]\n", programName);
}

int
parseInput(int argc, char **argv) {

	// Available options
	static struct option long_options[] = {
		//{name, (no_argument|required_argument|optional_argument), int *flag, val}
		{"jobs", optional_argument, 0, 'j'},
		{"help", no_argument,       0, 'h'},

		{0, 0, 0, 0}  // End of arguments
	};


	// Parsing loop
	opterr = 0;
	int getopt_ret, option_index;
	char *tail;

	for(;;) {
		getopt_ret = getopt_long(argc, argv, "hj::", long_options, &option_index);

		if (getopt_ret == -1)
			break;

		switch (getopt_ret) {
			case 0:
				break;

			case 'h':  // Help
				printHelp(argv[0]);
				exit(0);

			case 'j':  // Jobs
				if(optarg)
					MAX_JOBS = strtol(optarg, &tail, 0);
				else
					MAX_JOBS = sysconf(_SC_NPROCESSORS_ONLN);
				MAX_JOBS = MAX(MAX_JOBS, 1);
				printf("Using %d jobs\n", MAX_JOBS);
				break;

			case '?':
			default:
				printf("Unknown option\n");
				return 10;
		}
	}

	return 0;
}



// Core
// ----
int availableJobs = 1;  // Evil, but handy global state

void
runProgram(char* cmd) {

	printf("Command: '%s'\n", cmd);


	// TODO: wait (if required)

	// Parse program name and arguments
	int tokens = 1;
	char delim[] = " ";

	// Figure out the number of tokens to avoid realloc
	printf("Found %d tokens on '%s'\n", tokens, cmd);
	// ... or just use malloc and realloc when needed

	// Set up filename and arguments for execvp
	char *fileName, **args;
	// ...

	/*
	printf("Running: '%s'\n", fileName);
	for(size_t i=0; i<=tokens; i++)
		printf("  - (%p) '%s'\n", args[i], args[i]);
	*/

	// TODO: fork and exec(vp)

	// TODO: free any unused resources
}


int
main(int argCount, char** argValues) {

	// Parse arguments
	if (parseInput(argCount, argValues))
		exit(1);

	availableJobs = MAX_JOBS;

	// Read stdin
	char *line = NULL;
	size_t bufferSize;

	while (getline(&line, &bufferSize, stdin) != -1) {
			runProgram(line);
		FREE(line);
	}

	// getline allocates memory even on failure for consistency
	FREE(line);

	return 0;
}
